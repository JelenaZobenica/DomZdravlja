DROP SCHEMA IF EXISTS domzdravlja;
CREATE SCHEMA domzdravlja DEFAULT CHARACTER SET utf8;
USE domzdravlja;

CREATE TABLE pacijenti (
	id_pacijent INT AUTO_INCREMENT,
	lbo VARCHAR(11) UNIQUE NOT NULL,
	ime VARCHAR(20) NOT NULL,
	prezime VARCHAR(20) NOT NULL,
	datum_rodjenja DATE NOT NULL,
	PRIMARY KEY (id_pacijent)
);

CREATE TABLE tippregleda(
	id_tip INT AUTO_INCREMENT,
	naziv VARCHAR(20) NOT NULL,
	cena DOUBLE NOT NULL,
	PRIMARY KEY (id_tip)
);

CREATE TABLE pregledi (
	id_pregled INT AUTO_INCREMENT,
	datum_vreme DATETIME NOT NULL,
	id_tip INT NOT NULL,
	id_pacijent INT NOT NULL,
    placen BOOLEAN NOT NULL,
	PRIMARY KEY (id_pregled),
	
	FOREIGN KEY (id_tip) REFERENCES tippregleda(id_tip)
	    ON DELETE RESTRICT,
	 FOREIGN KEY (id_pacijent) REFERENCES pacijenti(id_pacijent)
	    ON DELETE RESTRICT   
);

INSERT INTO tippregleda (naziv, cena) VALUES ("opšti", 2000.00);
INSERT INTO tippregleda (naziv, cena) VALUES ("specijalistički", 3000.00);
INSERT INTO tippregleda (naziv, cena) VALUES ("dijagnostički", 10000.00);
INSERT INTO tippregleda (naziv, cena) VALUES ("laboratorijski", 500.00);
