package domzdravlja.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import domzdravlja.dao.PregledDAO;
import domzdravlja.dao.TipPregledaDAO;
import domzdravlja.model.Pregled;
import domzdravlja.model.TipPregleda;
import domzdravlja.utils.PomocnaKlasa;

public class TipPregledaUI {

	private static void ispisiTekstOpcije() {
		System.out.println("Rad sa tipovima pregleda - opcije:");
		System.out.println("\tOpcija broj 1 - izvestaj po tipu pregleda");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	public static void meni() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiTekstOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				izvestajPoTipu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	private static void izvestajPoTipu() {
		List<Pregled> sviPregledi = PregledDAO.getAll(ApplicationUI.getConn());
		List<Pregled> sviPreglediKojiPripadajuPeriodu = new ArrayList<>();
		Map<String, List<Pregled>> mapPreglediPoTipu = new HashMap<>();

		System.out.println("Unesite pocetak perioda u formatu yyyy-MM-dd hh:mm:ss");
		Date pocetak = PomocnaKlasa.ocitajDatum1();
		System.out.println("Unesite kraj perioda u formatu yyyy-MM-dd hh:mm:ss");
		Date kraj = PomocnaKlasa.ocitajDatum1();

		while (kraj.before(pocetak)) {
			System.out.println("Kraj perioda mora biti pre pocetka.");
			System.out.println("Unesite kraj perioda u formatu yyyy-MM-dd hh:mm:ss");
			kraj = PomocnaKlasa.ocitajDatum1();
		}

		sviPreglediKojiPripadajuPeriodu = preglediKojiPripadajuPeriodu(sviPregledi, pocetak, kraj);

		if (sviPreglediKojiPripadajuPeriodu.size() != 0) {
			mapPreglediPoTipu = sviPreglediKojiPripadajuPeriodu.stream()
					.collect(Collectors.groupingBy(Pregled::getNazivTipaPregleda));
		}
		System.out.println("Izvestaj za period " + PomocnaKlasa.sdf.format(pocetak) + " - " 
		+ PomocnaKlasa.sdf.format(kraj));
		ispisiIzvestaj(mapPreglediPoTipu);
	}

	private static List<Pregled> preglediKojiPripadajuPeriodu(List<Pregled> sviPregledi, Date pocetakPerioda,
			Date krajPerioda) {
		List<Pregled> prglediPripadajuPeriodu = new ArrayList<>();
		for (Pregled pregled : sviPregledi) {
			if (pregled.pregledPripadaPeriodu(pocetakPerioda, krajPerioda)) {
				prglediPripadajuPeriodu.add(pregled);
			}
		}
		return prglediPripadajuPeriodu;
	}

	private static void ispisiIzvestaj(Map<String, List<Pregled>> preglediPoTipuMap) {
		ispisiZaglavljeIzvestaja();
		ispisiTeloIzvestaja(preglediPoTipuMap);
	}

	private static void ispisiZaglavljeIzvestaja() {
		System.out.println("-------------------------------------------------------------------------");
		System.out.printf("%-16s %14s %11s %28s\n", "tip", "broj pregleda", "ukupna cena",
				"prosecna starost pacijenta");
		System.out.println("-------------------------------------------------------------------------");
	}

	private static void ispisiTeloIzvestaja(Map<String, List<Pregled>> preglediPoTipuMap) {
		int ukupanBrojPregledaSvihTipova = 0;
		double ukupnaSumaPregledaSvihTipova = 0;
		int ukupnaProsecnaStarostPacijentaSvihPregleda = 0;
		int brojRedovaUIzvestaju = preglediPoTipuMap.keySet().size();

		for (Entry<String, List<Pregled>> tipMap : preglediPoTipuMap.entrySet()) {
			String keyNazivTipa = tipMap.getKey();
			List<Pregled> preglediPoTipu = tipMap.getValue();

			ukupanBrojPregledaSvihTipova += preglediPoTipu.size();
			ukupnaSumaPregledaSvihTipova += izracunajSumuPregledaPoTipu(preglediPoTipu);
			int prosecnaStarost = (int) Math
					.round((double) izracunajUkupnuStarostPacijenata(preglediPoTipu) / preglediPoTipu.size());
			ukupnaProsecnaStarostPacijentaSvihPregleda += prosecnaStarost;

			System.out.printf("%-18s %-14d %-13.2f %-28d\n", keyNazivTipa, preglediPoTipu.size(),
					izracunajSumuPregledaPoTipu(preglediPoTipu), prosecnaStarost);
		}
		ukupnaProsecnaStarostPacijentaSvihPregleda = (int) Math
				.round((double) ukupnaProsecnaStarostPacijentaSvihPregleda / brojRedovaUIzvestaju);
		ispisiDnoIzvestaja(ukupanBrojPregledaSvihTipova, ukupnaSumaPregledaSvihTipova,
				ukupnaProsecnaStarostPacijentaSvihPregleda);
	}

	private static void ispisiDnoIzvestaja(int ukupanBrojPregleda, double ukupnaSumaPregleda,
			int ukupnaProsecnaStarostPacijenata) {
		System.out.println("-------------------------------------------------------------------------");
		System.out.printf("%-18s %-14d %-13.2f %-28d\n", "ukupno:", ukupanBrojPregleda, ukupnaSumaPregleda,
				ukupnaProsecnaStarostPacijenata);
		System.out.println("-------------------------------------------------------------------------");
	}

	private static int izracunajUkupnuStarostPacijenata(List<Pregled> preglediPoTipu) {
		int ukupnaStarost = preglediPoTipu.stream()
				.map(pregled -> pregled.getPacijent().getStarost())
				.reduce(0, (a, b) -> a + b);

		return ukupnaStarost;
	}

	private static double izracunajSumuPregledaPoTipu(List<Pregled> preglediPoTipu) {
		double sumaPoTipuPregleda = preglediPoTipu.stream()
				.map(pregled -> pregled.getTipPregleda().getCena())
				.reduce((double) 0, (a, b) -> a + b);

		return sumaPoTipuPregleda;
	}

	public static TipPregleda pronadjiTipPregleda(int idTipaPregleda) {
		TipPregleda tip = null;
		tip = TipPregledaDAO.getById(ApplicationUI.getConn(), idTipaPregleda);

		return tip;
	}

}
