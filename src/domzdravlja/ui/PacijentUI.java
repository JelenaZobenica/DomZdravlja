package domzdravlja.ui;

import java.util.Date;
import java.util.List;

import com.jakewharton.fliptables.FlipTable;

import domzdravlja.dao.PacijentDAO;
import domzdravlja.model.Pacijent;
import domzdravlja.model.Pregled;
import domzdravlja.utils.PomocnaKlasa;

public class PacijentUI {

	public static void ispisiTekstOpcije() {	
		System.out.println("Rad sa pacijentima - opcije:");
		System.out.println("\tOpcija broj 1 - dodavanje pacijenta");
		System.out.println("\tOpcija broj 2 - ispisi sve pacijente sa pregledima");
		System.out.println("\tOpcija broj 3 - ispisi sve pacijente sa pregledima Flip tables");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");	
	}

	public static void meni() {	
		int odluka = -1;
		while (odluka!= 0) {
			ispisiTekstOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz");	
					break;
				case 1:
					dodajPacijenta();
					break;
				case 2:	
					prikaziSvePacijenteINjegovePreglede();
					break;
				case 3:
					prikaziSvePacijenteINjegoveFlipTables();
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;	
			}
		}
	}

	private static void prikaziSvePacijenteINjegovePreglede() {
		List<Pacijent> sviPacijenti = PacijentDAO.getAll(ApplicationUI.getConn());
		
		for(Pacijent pacijent : sviPacijenti) {
			System.out.println(pacijent);
			for(Pregled pregled : pacijent.getSviPregledi()) {
				System.out.println("\t" + pregled);
			}
			System.out.println();
		}
	}
	
	private static void prikaziSvePacijenteINjegoveFlipTables() {
		List<Pacijent> sviPacijenti = PacijentDAO.getAll(ApplicationUI.getConn());
		// zaglavlje za Flip table biblioteku
		String[] headers = { "Datum i vreme pregleda", "Naziv tipa", "Cena", "Placen" };
	
		for(Pacijent pacijent : sviPacijenti) {
			System.out.println(pacijent);
			int brojRedova = pacijent.getSviPregledi().size();
			int brojKolona = headers.length;
			// podaci za Flip table biblioteku
			String[][] data = new String[brojRedova][brojKolona];
			for (int i = 0; i < pacijent.getSviPregledi().size(); i++)  {
				int indeksKolone = 0;
				Pregled pregled = pacijent.getSviPregledi().get(i);
				String datumIVreme = PomocnaKlasa.sdf.format(pregled.getDatumVreme());
				String placenPregled = pregled.isPlacen() == true ? "da" : "ne";
				data[i][indeksKolone++] = datumIVreme;
				data[i][indeksKolone++] = pregled.getTipPregleda().getNaziv();
				data[i][indeksKolone++] = pregled.getTipPregleda().getCena() + "";
				data[i][indeksKolone++] = placenPregled;
			}
			System.out.println(FlipTable.of(headers, data));
		}
	}

	private static void dodajPacijenta() {
		System.out.println("Unesite lbo: ");
		long lbo = PomocnaKlasa.ocitajLongBroj();
		String LBO = lbo+"";
		
		while (LBO.length() != 11) {
			System.out.println("LBO mora sadrzati 11 cifara.");
			System.out.println("Unesite lbo: ");
			lbo = PomocnaKlasa.ocitajLongBroj();
			LBO = lbo+"";
		}
		
		System.out.println("Unesi ime: ");
		String ime = PomocnaKlasa.ocitajTekst();
		System.out.println("Unesi prezime: ");
		String prezime = PomocnaKlasa.ocitajTekst();
		System.out.println("Unesite datum rodjenja u formatu yyyy-MM-dd: ");
		Date datumRodjenja = PomocnaKlasa.ocitajDatumRodjenja();
		
		while(datumRodjenja.after(new Date())) {
			System.out.println("Datum rodjenja mora biti manji ili jednak danasnjem");
			System.out.println("Unesite datum rodjenja u formatu yyyy-MM-dd: ");
			datumRodjenja = PomocnaKlasa.ocitajDatumRodjenja();
		}
		
		Pacijent pacijent = new Pacijent(0, LBO, ime, prezime, datumRodjenja);
		boolean dodat = PacijentDAO.add(ApplicationUI.getConn(), pacijent);
		String subObavestenje = dodat == true ? "je " : "nije "; 
		String porukaODodavanju = "Pacijent "+ subObavestenje + "uspesno dodat";
		System.out.println(porukaODodavanju);
	}
	
	public static Pacijent pronadjiPacijenta(int id) {
		Pacijent pacijent = null;
		pacijent = PacijentDAO.getByID(ApplicationUI.getConn(), id);
		
		return pacijent;
	}
}
