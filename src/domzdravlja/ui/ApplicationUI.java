package domzdravlja.ui;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import domzdravlja.utils.PomocnaKlasa;

public class ApplicationUI {

	private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/domzdravlja?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}

	public static Connection getConn() {
		return conn;
	}

	private static void ispisiTekstOpcije() {	
		System.out.println("Dom zdravlja - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa pacijentima");
		System.out.println("\tOpcija broj 2 - Rad sa pregledima");
		System.out.println("\tOpcija broj 3 - Rad sa tipovima pregleda");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");	
	}

	public static void meni() {
		int odluka = -1;
		while (odluka!= 0) {
			ispisiTekstOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz iz programa");	
					break;
				case 1:
					PacijentUI.meni();
					break;
				case 2:
					PregledUI.meni();
					break;
				case 3:
					TipPregledaUI.meni();
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;
			}
		}
	}

	public static void main(String[] args) throws IOException {	
		meni();

		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		System.out.print("Program izvrsen");
	}

}
