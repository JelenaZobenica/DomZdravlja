package domzdravlja.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domzdravlja.dao.PacijentDAO;
import domzdravlja.dao.PregledDAO;
import domzdravlja.model.Pacijent;
import domzdravlja.model.Pregled;
import domzdravlja.model.TipPregleda;
import domzdravlja.utils.PomocnaKlasa;

public class PregledUI {

	private static void ispisiTekstOpcije() {	
		System.out.println("Rad sa pregledima - opcije:");
		System.out.println("\tOpcija broj 1 - dodaj pregled");
		System.out.println("\tOpcija broj 2 - placanje pregleda");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");	
	}

	public static void meni() {	
		int odluka = -1;
		while (odluka != 0) {
			ispisiTekstOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
				case 0:
					System.out.println("Izlaz");	
					break;
				case 1:
					dodajPregled();
					break;
				case 2:
					placanjePregleda();
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;	
			}
		}
	}

	private static void placanjePregleda() {
		Pacijent pacijent = pronadjiPacijentaById();
		double sumaZaPlacanje = 0;
		List<Pregled> preglediZaPlacanje = new ArrayList<>();
		
		System.out.printf("%2s %-20s %-20s %4s\n", "Id", "Datum", "Tip pregleda", "Cena");
		System.out.println("---------------------------------------------------");
		for(Pregled pregled : pacijent.getSviPregledi()) {
			if(!pregled.isPlacen()) {
				preglediZaPlacanje.add(pregled);
				System.out.printf("%-2d %-20s %-20s %4s\n", pregled.getId(), 
						PomocnaKlasa.sdf.format(pregled.getDatumVreme()), 
						pregled.getTipPregleda().getNaziv(), pregled.getTipPregleda().getCena());
				sumaZaPlacanje += pregled.getTipPregleda().getCena();
			}
		}
		System.out.println("---------------------------------------------------");
		System.out.println("Ukupan iznos neplacenih pregleda: " + sumaZaPlacanje);
		
		if (PomocnaKlasa.ocitajOdlukuOPotvrdi("da platite preglede") == 'Y') {
			boolean placen = true;
			for(Pregled pregled : preglediZaPlacanje) {
				pregled.setPlacen(placen);
				PregledDAO.update(ApplicationUI.getConn(), pregled);
			}
			System.out.println("Pregledi su uspesno placeni");
		}
	}
	
	private static Pacijent pronadjiPacijentaById() {
		List<Pacijent> sviPacijenti = PacijentDAO.getAll(ApplicationUI.getConn());
		Pacijent pacijent = null;
		
		while(pacijent == null) {
			System.out.println("Unesite id pacijenta: ");
			int idPacijenta = PomocnaKlasa.ocitajCeoBroj();
			for(Pacijent p : sviPacijenti) {
				if(p.getId() == idPacijenta) {
					pacijent = p;
					break;
				}
			}
		}
		
		return pacijent;
		
	}

	private static void dodajPregled() {
		System.out.println("Unesite id pacijenta: ");
		int idPacijenta = PomocnaKlasa.ocitajCeoBroj();
		Pacijent pacijent = PacijentUI.pronadjiPacijenta(idPacijenta);
		
		System.out.println("Unesite id tipa pregleda:");
		int idTipaPregleda = PomocnaKlasa.ocitajCeoBroj();
		TipPregleda tip = TipPregledaUI.pronadjiTipPregleda(idTipaPregleda);
		
		Date datumVreme = new Date();
		boolean placen = false;
		Pregled pregled = null;
		
		if(pacijent != null && tip != null) {
			pregled = new Pregled(0, datumVreme, tip, pacijent, placen);
			boolean dodat = PregledDAO.add(ApplicationUI.getConn(), pregled);
			String subObavestenje = dodat == true ? "je " : "nije "; 
			String porukaODodavanju = "Pregled "+ subObavestenje + "uspesno dodat";
			System.out.println(porukaODodavanju);
		} else {
			System.out.println("Nije moguce dodati pregled.");
			if(pacijent == null && tip == null)
				System.out.println("Pacijent i tip pregleda ne postoje u evidenciji.");
			else if(pacijent == null)
				System.out.println("Pacijent ne postoje u evidenciji.");
			else
				System.out.println("Tip pregleda ne postoje u evidenciji.");
			
		}
		
	}
}
