package domzdravlja.model;

import java.util.ArrayList;
import java.util.Date;

import domzdravlja.utils.PomocnaKlasa;

public class Pacijent {

	private int id;
	private String lbo;
	private String ime;
	private String prezime;
	private Date datumRodjenja;
	
	ArrayList<Pregled> sviPregledi = new ArrayList<>();

	public Pacijent(int id, String lbo, String ime, String prezime, Date datumRodjenja) {
		super();
		this.id = id;
		this.lbo = lbo;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
	}
	
	@Override
	public String toString() {
		return "Pacijent [id=" + id + ", lbo=" + lbo + ", ime=" + ime
		+ ", prezime=" + prezime + ", datum rodjenja=" + PomocnaKlasa.sdf1.format(datumRodjenja) + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pacijent other = (Pacijent) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public int getStarost() {
		long datumRodj = datumRodjenja.getTime();
		long danasnjiDatum = new Date().getTime();
		long razlika = danasnjiDatum - datumRodj;
		int razlikaUDanima = (int) Math.round((double) razlika/(1000 * 60 * 60 * 24));
		int brojDanaUGodini = 365;
		int starost = razlikaUDanima / brojDanaUGodini;
		
		return starost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLbo() {
		return lbo;
	}

	public void setLbo(String lbo) {
		this.lbo = lbo;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public ArrayList<Pregled> getSviPregledi() {
		return sviPregledi;
	}

	public void setSviPregledi(ArrayList<Pregled> sviPregledi) {
		this.sviPregledi = sviPregledi;
	}

	
}
