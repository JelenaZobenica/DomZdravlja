package domzdravlja.model;

import java.util.Date;

import domzdravlja.utils.PomocnaKlasa;

public class Pregled {

	private int id;
	private Date datumVreme;
	private TipPregleda tipPregleda;
	private Pacijent pacijent;
	private boolean placen;
	
	public Pregled(int id, Date datumVreme, TipPregleda tipPregleda, Pacijent pacijent, boolean placen) {
		super();
		this.id = id;
		this.datumVreme = datumVreme;
		this.tipPregleda = tipPregleda;
		this.pacijent = pacijent;
		this.placen = placen;
	}
	
	@Override
	public String toString() {
		String placenPregled = placen == true ? "da" : "ne";
		return "Pregled [datum i vreme =" + PomocnaKlasa.sdf.format(datumVreme) 
		+ ", naziv tipa pregleda=" + tipPregleda.getNaziv() + ", cena tipa pregleda=" 
		+ tipPregleda.getCena() + ", placen=" + placenPregled + "]";
	}
	
	public boolean pregledPripadaPeriodu(Date pocetak, Date kraj) {
		boolean pripada = false;
		
		if ((datumVreme.after(pocetak) || datumVreme.equals(pocetak)) 
				&& (datumVreme.before(kraj) || datumVreme.equals(kraj)))
			pripada = true;
		
		return pripada;
	}
	
	public String getNazivTipaPregleda() {
		return tipPregleda.getNaziv();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatumVreme() {
		return datumVreme;
	}

	public void setDatumVreme(Date datumVreme) {
		this.datumVreme = datumVreme;
	}

	public TipPregleda getTipPregleda() {
		return tipPregleda;
	}

	public void setTipPregleda(TipPregleda tipPregleda) {
		this.tipPregleda = tipPregleda;
	}

	public Pacijent getPacijent() {
		return pacijent;
	}

	public void setPacijent(Pacijent pacijent) {
		this.pacijent = pacijent;
	}

	public boolean isPlacen() {
		return placen;
	}

	public void setPlacen(boolean placen) {
		this.placen = placen;
	}
	
	

}
