package domzdravlja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domzdravlja.model.Pacijent;
import domzdravlja.model.Pregled;
import domzdravlja.ui.ApplicationUI;
import domzdravlja.utils.PomocnaKlasa;

public class PacijentDAO {
	
	public static Pacijent getByID(Connection conn, int id) {
		Pacijent pacijent = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT lbo, ime, prezime, datum_rodjenja FROM pacijenti WHERE id_pacijent = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, id);
			rset = pstmt.executeQuery();

			while (rset.next()) {
				int indeks = 1;
				String lbo = rset.getString(indeks++);
				String ime = rset.getString(indeks++);
				String prezime = rset.getString(indeks++);
				Date datumRodjenja = null;
				try {
					datumRodjenja = PomocnaKlasa.sdf1.parse(rset.getString(indeks++));
				} catch (ParseException e) {
					System.out.println("Graska kod parsiranja datuma iz baze.");
				}
				
				pacijent = new Pacijent(id, lbo, ime, prezime, datumRodjenja);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return pacijent;
	}
	
	public static boolean add(Connection conn, Pacijent pacijent) {
		PreparedStatement pstmt = null;
		
		try {
			String query = "INSERT INTO pacijenti (lbo, ime, prezime, datum_rodjenja) VALUES (?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, pacijent.getLbo());
			pstmt.setString(index++, pacijent.getIme());
			pstmt.setString(index++, pacijent.getPrezime());
			pstmt.setString(index++, PomocnaKlasa.sdf1.format(pacijent.getDatumRodjenja()));

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static List<Pacijent> getAll(Connection conn) {
		List<Pacijent> sviPacijenti = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id_pacijent, lbo, ime, prezime, datum_rodjenja FROM pacijenti";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String lbo = rset.getString(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				Date datumRodjenja = null;
				try {
					datumRodjenja = PomocnaKlasa.sdf1.parse(rset.getString(index++));
				} catch (ParseException e) {
					System.out.println("Greska kod parsiranja datuma iz baze.");
				}
				
				Pacijent pacijent = new Pacijent(id, lbo, ime, prezime, datumRodjenja);
				List<Pregled> preglediPacijenta = PregledDAO.getAllByPacijent(ApplicationUI.getConn(), pacijent);
				pacijent.getSviPregledi().addAll(preglediPacijenta);
				sviPacijenti.add(pacijent);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sviPacijenti;
	}
	
}