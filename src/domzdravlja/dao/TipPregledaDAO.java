package domzdravlja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import domzdravlja.model.TipPregleda;

public class TipPregledaDAO {

	public static TipPregleda getById(Connection conn, int id) {
		TipPregleda tipPregleda = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT naziv, cena FROM tippregleda WHERE id_tip = ?";

			pstmt = conn.prepareStatement(query);
			int indeks = 1;
			pstmt.setInt(indeks, id);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				String naziv = rset.getString(index++);
				double cena = rset.getDouble(index++);
				
				tipPregleda = new TipPregleda(id, naziv, cena);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return tipPregleda;
	}

}

