package domzdravlja.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import domzdravlja.model.Pacijent;
import domzdravlja.model.Pregled;
import domzdravlja.model.TipPregleda;
import domzdravlja.ui.ApplicationUI;
import domzdravlja.utils.PomocnaKlasa;

public class PregledDAO {
	
	public static boolean add(Connection conn, Pregled pregled) {
		PreparedStatement pstmt = null;
		
		try {
			String query = "INSERT INTO pregledi (datum_vreme, id_tip, id_pacijent, placen) VALUES (?, ?, ?,?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, PomocnaKlasa.sdf.format(pregled.getDatumVreme()));
			pstmt.setInt(index++, pregled.getTipPregleda().getId());
			pstmt.setInt(index++, pregled.getPacijent().getId());
			pstmt.setBoolean(index++, pregled.isPlacen());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static List<Pregled> getAllByPacijent(Connection conn, Pacijent pacijent) {
		List<Pregled> sviPreglediPacijenta = new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id_pregled, datum_vreme, id_tip, placen FROM pregledi WHERE id_pacijent=?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, pacijent.getId());
			rset = pstmt.executeQuery();

			while (rset.next()) {
				int indeks = 1;
				int idPregled = rset.getInt(indeks++);
				Date datumVreme = null;
				try {
					datumVreme = PomocnaKlasa.sdf.parse(rset.getString(indeks++));
				} catch (ParseException e) {
					System.out.println("Greska prilikom parsiranja datuma iz baze.");
				}
				int id_tip = rset.getInt(indeks++);
				TipPregleda tip = TipPregledaDAO.getById(ApplicationUI.getConn(), id_tip);
				boolean placen = rset.getBoolean(indeks++);
				Pregled pregled = new Pregled(idPregled, datumVreme, tip, pacijent, placen);
				sviPreglediPacijenta.add(pregled);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sviPreglediPacijenta;
	}
	
	public static boolean update(Connection conn, Pregled pregled) {
		PreparedStatement pstmt = null;
		
		try {
			String query = "UPDATE pregledi SET placen = ? WHERE id_pacijent = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setBoolean(index++, pregled.isPlacen());
			pstmt.setInt(index++, pregled.getPacijent().getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static List<Pregled> getAll(Connection conn) {
		List<Pregled> sviPregledi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id_pregled, datum_vreme, id_tip, id_pacijent, placen FROM pregledi ORDER BY id_tip";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				Date datumVreme = null;
				try {
					datumVreme = PomocnaKlasa.sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					System.out.println("Greska kod parsiranja datuma iz baze.");
				}
				int idTipa = rset.getInt(index++);
				TipPregleda tip = TipPregledaDAO.getById(ApplicationUI.getConn(), idTipa);
				int idPacijenta = rset.getInt(index++);
				Pacijent pacijent = PacijentDAO.getByID(ApplicationUI.getConn(), idPacijenta);
				boolean placen = rset.getBoolean(index++);
			
				
				Pregled pregled = new Pregled(id, datumVreme, tip, pacijent, placen);
				sviPregledi.add(pregled);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sviPregledi;
	}

}
